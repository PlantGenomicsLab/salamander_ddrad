library(tidyverse)
library(dplyr)
library(readxl)
library(stringr)

relationship <- read.csv("Data/urban/CGI2Urban_Relationship.csv")

egg_color <- read_excel("Data/urban/meta_gens_05212024_white_coords.xlsx", sheet=1)

relationship_eggcolor <- left_join(relationship, egg_color, by=c("CGI_ID"="Sample_ID"))

pond_coordinates <- read_excel("Data/urban/meta_gens_05212024_white_coords.xlsx", sheet=2)


# Define the matching criteria for ponds
pond_criteria <- data.frame(
  PondID = c("Atwood", "3 way sedge", "AA006", "Borrow Pit 2", "Boulder", "Brookside", "C6",
             "Creepy","Dead Turkey", "Dentist", "E1", "E8", "Ericas", "Fence", "FrogBog", "FU22", "G1", "Ginnas", "Greenbriar", "Kealoha's",
             "Laurel", "Long", "Mill", "Nathaniel's Pond", "Pretty Beaver Pond", "Pussy Willow", "Quarry", "Saddle", "Shrub", "T2", "T5", "Underground Stream",
             "Walker", "Westford", "Woodpile", "Y6", "Freaky"),
  Pattern = c("AMMAAT|AMAT|MASAT", "AMMA3WS", "AMMAAA006", "AMMABP|AMBP", "AMMABLDR|AMBLD|MASBLR", "AMMABR", "AMMAC6|AMC6|MASC6",
              "AMMACR|AMCR|MASCR", "AMMADT|AMDT|MASDT","AMMADEN","AMMAE1","AMMAE8|AME8|MASE8", "AMMAErica", "AMMAFence|AMFEN|MASFEN",
              "AMMAFB", "AMMAFU22|AMFU22|MASFU", "ammag1", "AMMAGinna|AMGNA|MASGNA", "AMMAGB|AMGB|MASGB", "ammakEALOHA", 
              "ammalAUREL|AMMALaurel|AMMALAUREL|AMLRL|MASLRL",
              "AMMALong|AMLNG|MASLNG", "AMMAMill|AMMILL|MASMLL", "ammana|AMNATH|MASNAT", "AMMAPBP|ammapbp", "AMMAPW", "AMMAQuarry|AMQURY|MASQUA",
              "AMMASaddle|AMSADL|AMSDL|MASSDL", "AMMAShrub|AMSHR|MASSHR", "AMAT28","AMMAT5|AMT5|MAST5", "AMMAUS", "AMMAWALK|AMWLK|MASWLK",
              "AMMAWEST|AMWEST|MASWST", "AMMAWP|AMWP|MASWP", "AMMAY6", "AMMAFreaky")  # Regex pattern to match any of these substrings
)

popmap <- relationship %>%
  rowwise() %>%
  mutate(PondID = ifelse(
    any(str_detect(CGI_ID , pond_criteria$Pattern)), 
    pond_criteria$PondID[which.max(str_detect(CGI_ID, pond_criteria$Pattern))],
    "Unknown"
  ))

#View(popmap)


pond_coordinates <- pond_coordinates %>% select("Pond ID", "Latitude", "Longitude", "Treatment")

full_popmap <- left_join(popmap, pond_coordinates, by = c("PondID"="Pond ID")) %>% 
  mutate(years = str_extract(DNA_Plate_ID, "(19|20)\\d{2}"))

View(full_popmap)

write.csv(x = full_popmap, file = "Data/urban/meta_ponds_eggcolor_10_24_2024_v2.csv")


View(full_popmap %>% filter(PondID == "Unknown"))

# Get some pop summary counts
grouped_popmap <- full_popmap %>% group_by(PondID, Treatment, years) %>% dplyr::summarise(n=n())

View(grouped_popmap)
