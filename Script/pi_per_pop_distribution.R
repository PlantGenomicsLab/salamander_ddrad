#
# Nucleotide diversity vis
#
# Load necessary libraries
library(ggplot2)
library(dplyr)

# Function to read a .out file and calculate average and SD of nucleotide diversity
calculate_diversity_stats <- function(file_path) {
  # Read the file
  data <- read.table(file_path, header = TRUE)
  
  # Fourth column contains stats
  nucleotide_diversity <- data[, 4]
  
  # Calculate average and standard deviation
  avg_diversity <- mean(nucleotide_diversity, na.rm = TRUE)
  sd_diversity <- sd(nucleotide_diversity, na.rm = TRUE)
  
  # Return the results as a data frame
  return(data.frame(average = avg_diversity, sd = sd_diversity))
}

# Get a list of all .out files in the directory
files <- list.files(path = "./Output/Data/pi", pattern = "_sequenceDiversity.out$", full.names = TRUE)

# Initialize an empty data frame to store results
results <- data.frame(population = character(), average = numeric(), sd = numeric())

# Loop through each file and calculate stats
for (file in files) {
  # Extract the population name (before the first '_')
  population_name <- sub("_.*$", "", basename(file))
  
  # Calculate the average and standard deviation of nucleotide diversity
  stats <- calculate_diversity_stats(file)
  
  # Add population name to the results
  stats$population <- population_name
  
  # Append to results
  results <- rbind(results, stats)
}

# Sort the results by average nucleotide diversity (from highest to lowest)
results$population <- factor(results$population, levels = results$population[order(-results$average)])

# Plot the results
ggplot(results, aes(x = average, y = population)) +
  geom_point(size = 3) +
  geom_errorbarh(aes(xmin = average - sd, xmax = average + sd), height = 0.2) +
  labs(title = "Population Nucleotide Diversity (Pi)",
       x = "Nucleotide Diversity (Pi) Average ± SD",
       y = "Population") +
  theme_minimal() +
  theme(legend.position = "none") +
  theme(axis.text.y = element_text(size = 10))
  theme(axis.text.y = element_text(size = 10))
  
ggsave(filename="perPondAveragePi.png", plot=last_plot(),device = "png")
