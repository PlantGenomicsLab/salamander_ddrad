##################
# FASTP ANALYSIS #
##################
# Summarise json #
##################
#                #
##################
library(tidyverse)
library(rjson)
library(tools)
library(ggplot2)

#
# Function to read JSON files and fix incomplete final lines
#
read_and_fix_json <- function(file_path) {
  lines <- readLines(file_path, warn = FALSE)
  if (length(lines) > 0 && !grepl("\\n$", last(lines))) {
    lines <- c(lines, "")  # Ensure the last line is complete
  }
  paste(lines, collapse = "\n")
}


collectFastpStatsIntoTibble <- function(dir="") {
  
  #
  # Initialize fastp tibble
  #
  fastp_tibble <- tibble(
    individual = character(),
    before_filter_reads = numeric(),
    after_filter_reads = numeric(),
    duplication_rate = numeric(),
    adapter_trimmed_reads = numeric()
  )
  
  json_list <- list.files(path=dir,pattern = "\\.json$", full.names=TRUE)
  
    for (json_file in json_list) {
    json_content <- read_and_fix_json(json_file)
    # Individual name
    indv <- gsub("_T1.fastp", "", file_path_sans_ext(basename(json_file)))
    
    # Read JSON File
    json_data <- fromJSON(paste(json_content, collapse=""))
    
    # Grab Stats
    before_filter_read_count <- json_data$summary$before_filtering$total_reads
    after_filter_read_count <- json_data$summary$after_filtering$total_reads
    percent_dup <- json_data$duplication$rate
    adapter_cont <- json_data$adapter_cutting$adapter_trimmed_reads
    
    fastp_tibble <- fastp_tibble %>%
      add_row(
        individual = indv,
        before_filter_reads = before_filter_read_count,
        after_filter_reads = after_filter_read_count,
        duplication_rate = percent_dup,
        adapter_trimmed_reads = adapter_cont
      )
    }
  return(fastp_tibble)
}


