<img align="left" width="160" height="90" src="media/marbledLarva_eating_spottedHatchling_sal.jpg" alt="Resume application project app icon">

# How Marbled Salamander Invasions Shape Spotted Salamander Genetic Diversity.

#### Experimental Design

This study aims to assess the impact of introducing marbled salamanders on genetic diversity in natural pond ecosystems by conducting a before-after, control-impact experimental design. Ten ponds were selected without a history of marbled salamanders and demonstrate genetic differientiated between eachother, alongside five controls ponds where no Marbled salamanders were introduced. Depending on the level of existing variation within a pond, egg masses from locally adapted populations were supplemented. Using Double digest restriction-site associated DNA sequences this study will characterize genomic variability, estimate gene flow, assign immigrants to nearby populations, and search for loci under selection. Forty individuals were sampled from each of the fifteen study populations and ten neighboring ponds whose individuals potentially immigrate to these study ponds. Sampling events from this same set occured a year and four years after to understand short and long-term selection respectively.

## Extractions Progress Report

- 2018 Pre selection Samples Analysis (762 samples) [▓▓▓▓░░░░] 50%

- 2018 Short-term selection Analysis (5 plates) [░░░░░░░░░░] 0%
    - Ready for Sequencing

- 2021 Long-term selection Analysis (5 plates)[░░░░░░░░░░] 0%
    - Extractions [░░░░░░░░░░] 0% 

### Study Site

<figure>
    <img src="Output/Plots/LandscapeGenomicsStudySite.jpg">
    <figcaption><strong>Figure 1.</strong> Study area in northeastern Connecticut where salamander eggs where salamander egg sampling was conducted across wetland habitats where a predator was present or absent.</figcaption>
</figure>


### Fastp Results For 2018 Samples

<figure>
  <img src="Output/Plots/fastp_after_filter_read_distribution.jpg" alt="Read Distribution">
  <figcaption><strong>Figure 2:</strong> After filtering read distribution.</figcaption>
</figure>

<figure>
    <img src="Output/Plots/fastp_per_pond_total_reads_after_filtering.jpg" alt="Per-Pond Read Distribution">
    <figcaption><strong>Figure 3:</strong> Per-Pond after filtering read distribution</figcaption>
</figure>


### Denovo Reference 

Chose a subset of fifty samples to be used in denovo reference construction ([high-throughput samples](Output/Data/Temp/high_read_count_50_samples_for_denovo_construction.txt)). 

<figure>
    <img src="Output/Plots/denovo_parameter_mixing_sequences.jpg" alt="Denovo paramter mixing resultant total sequences">
    <figcaption><strong>Figure 4.</strong> Parameter mixing of minimum sequence threshold m (between individuals) and n (within indiviual) and sequence similarity on total number of sequences in denovo reference sequences. sequence similarity is a parameter adjusted through cd-hitest and determines the amount of over/under - splitting. A higher sequence similarity shreshold would result in more splitting of sequences whereas a lower threshold would result in under splitting or clustering of sequences. This is an important step to optimize as it </figcaption>
</figure>

#### More missing genotypes when clustering reads with 90% sequence similarity. 




### Genotyping Results

**Table 2.** radseq denovo reference variant calling results between reference genomes with different filtering parameters of minimum reads within an individual and minimum number of reads between individual to be used in cd-hit read clustering. Also provided is the *raw* number of variants outputed by bcftools mpileup and after removing variants based on filters. *fitlers* in this instance mean removing genotypes not present in atleast 90% of individuals, alleles not present in atleast 15 individuals, minimum variant phred quality score of 20, and a minimum variant site read depth sum of 4. For denovo reference constructed by a single individual more stingent filters were applied. Sites were removed based on not being present in 50 individuals, minimum quality of 50, minimum cumulative site depth of 50.    

|indv| min reads within indv.| min reads between indv. | number of variant calls | after filtering* | average missing genotypes per site after filter | 
|---|-----------|-------------------------|---|---|---|
|50|  8 | 8 | 46103865 | 25625 | 0.97 |
|50|  8 | 24 | 11224630 | 9273 | null |
|1 | 20 | 1  | 347930 | 2304 | 0.40 |

<figure>
    <img src="Output/Plots/lm_fmiss_fastp.jpeg" alt="the effects of reads on fraction genotype missing">
    <figcaption> <strong>Figure 5.</strong> The effects of total reads after filtering on the amount of fraction genotype missing after variant filtering. Prior to calculating fraction genotype missingness variants were retain for high quality (x>50), sufficient site read depth (x>50), and minor allele count (x>50). This final variant call file contained <strong>2304</strong> sites that provided enough resolution to determine problematic samples shown above in red. Individuals in read are suspect for removal and filters re-applied to help retain more sites.
    </figcaption>
</figure>

---
**NOTE**

 More genotypes were retained after filtering for a radseq denovo reference genome with more relaxed minimum read thresholds (8,8).

---

#### The Effects of Removing Samples on Number of Sites Retained

[Cerca et al., 2021](https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/2041-210X.13562) describes how removing low-performing samples aids in the retention of more variant sites. To explore how this endeavor may help increase the number of shared sites I removed samples based on a range of missingness thresholds from weak x=0.5 to strong x=0.1. After removing these samples I re-applied site-based filters such as qual>40, info/dp>10, format/dp>8, maf>0.05, and fmiss<.3.

<figure>
    <img src="Output/Plots/Urban_20_1_variant_filtering_range.jpeg">
    <figcaption> <strong>Figure 6.</strong> Variant counts (red) and individuals (blue) after removing individuals across a range of genotype missingness thresholds.
    </figcaption>
</figure>

#### Samples Per-Pond After Removing High Missingness

<figure>
    <img src="Output/Plots/per_pond_sample_counts.jpg" alt="Stack bar chart, samples filtered over total number of samples per pond">
    <figcaption> <strong>Figure 7.</strong> Proportion of samples retained for analysis in grey to number filtered in black per pond. 
    </figcaption>
</figure>

### Population Structure

#### Principal Component analysis

<figure>
    <img src="Output/Plots/Urban_20_1_plink_400_14_20perc_pca.jpeg">
    <figcaption><strong>Figure 8.</strong> Principal component analysis of 2018 samples for landscape genomics study. First and seconds axis explain 19 and 16 percent of genetic variation respectively. 
    </figcaption>
<figure>


#### Genome-wide association with plink --logistics

| Treatment | Counts |
|-----------|--------|
| White     | 28     |
| Black     | 441    |

<figure>
    <img src="Output/Plots/Urban_20_1_2431_plink_logistics_manhattan.png">
    <figcaption><strong>Figure 7.</strong> Manhattan plot from logistic regression run in plink.
</figure>



| Plate | Status |
|-------|--------|
| 2018AMMAPS1 | sequenced |
| 2018AMMAPS2 | sequenced |
| 2018AMMAPS3 | sequenced |
| 2018AMMAPS4 | sequenced |
| 2018AMMAPS5 | sequenced |
| 2018AMMAPS6 | sequenced |
| 2018AMMAPS7 | sequenced |
| 2018AMMAPS8 | sequenced |
| AMMA_AS_CG2017 | sequenced |
| AMMA_AS_CG2017.2 | sequenced |
| AMMA_AS_CG2017.3 | sequenced |
| AMMA_AS_CG2017.4 | ready for sequencing |
| AMMA_AS_CG2017.5 | ready for sequencing |
| AMMA_2021_1 | needs to be extracted |
| AMMA_2021_2 | needs to be extracted |
| AMMA_2021_3 | needs to be extracted |
| AMMA_2021_4 | needs to be extracted |
| AMMA_2022_5 | needs to be extracted |


### Reposityory Layout

```
├───Data # Raw Data
│   ├───cgi # sourced from cgi at UCONN
│   │   ├───ddRAD_Adapters_UMI_PS_samplesheets
│   │   ├───gels
│   │   │   ├───05_01_2024
│   │   │   └───05_10_2024
│   │   │       └───quantifications
│   │   └───qc # List of samples that pass QC for 2017
│   ├───radseq # Output from NextFlow radseq 
│   │   └───fastp
│   └───urban # Sourced from Urban Lab
├───Output # Code output
│   ├───Data
│   │   └───barcodes
│   └───Plots
├───Script
    └───Functions
```


| Plate | Status |
|-------|--------|
| 2018AMMAPS1 | sequenced |
| 2018AMMAPS2 | sequenced |
| 2018AMMAPS3 | sequenced |
| 2018AMMAPS4 | sequenced |
| 2018AMMAPS5 | sequenced |
| 2018AMMAPS6 | sequenced |
| 2018AMMAPS7 | sequenced |
| 2018AMMAPS8 | sequenced |
| AMMA_AS_CG2017 | ready for sequencing |
| AMMA_AS_CG2017.2 | ready for sequencing |
| AMMA_AS_CG2017.3 | ready for sequencing |
| AMMA_AS_CG2017.4 | ready for sequencing |
| AMMA_AS_CG2017.5 | ready for sequencing |
| AMMA_2021_1 | needs to be extracted |
| AMMA_2021_2 | needs to be extracted |
| AMMA_2021_3 | needs to be extracted |
| AMMA_2021_4 | needs to be extracted |
| AMMA_2022_5 | needs to be extracted |

<p align="center">
    <img src="figures/tapestationResults_perPlate.png" alt="Figure" title="Figure 1">
</p>

<p align="center">
    <string>Figure 1</strong>: proportion of samples pass/warning/fail/missing per plate. Each plate has 6 pools with 16 samples, comprising a total of 96 samples per plate. 2018 samples have already been sequenced by CGI, therefore, pass/fail criteria is based on achieving a minimum total read count of 1,000,000 (million). For the 2018 samples, a dip in the number of counts indicates missing/empty wells. For 2017, 2021, and 2021 plates pass/fail criteria is based on tapestation results from CGI. 
</p>









